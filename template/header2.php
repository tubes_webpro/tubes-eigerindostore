<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <title><?php echo $judul ?></title>
</head>

<body>
<script type="text/javascript">
  $breadcrumb-divider: quote(">");
</script>

<nav class="navbar navbar-expand-lg navbar-light bg-white border-bottom">
	<a class="navbar-brand ml-5" href="<?= base_url(); ?>"><p>IndoStore</p></a>
	
<div class="collapse navbar-collapse mx-auto">
    <ul class="navbar-nav mr-5">
      <li class="nav-item mr-5">
        <a class="nav-link" href="<?= base_url(); ?>Home/katalog">MOUNTAINEERING<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item mr-5">
        <a class="nav-link" href="<?= base_url(); ?>Home/katalog1">1989</a>
      </li>
      <li class="nav-item mr-5">
        <a class="nav-link" href="<?= base_url(); ?>Home/katalog2">RIDING</a>
      </li>
    </ul>
</div>

<div class="nav">
	<div class="nav-item dropdown ml-2 mr-2">
        <a class="nav-link " href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"data-offset="20,30">
          <img src="<?php echo base_url(); ?>/assets/icon-src.png" style="width: 20px; height: 20px;">
        </a>		
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
  			<form class="form-inline">
    			<input class="form-control mx-auto" type="search" placeholder="Search" aria-label="Search">
    			<button class="btn btn-outline-success mx-auto" type="submit">Search</button>
  			</form>
      	</div>
	</div>

		<div class="nav-item dropdown ml-2 mr-2">
        <a class="nav-link " href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="<?php echo base_url(); ?>/assets/icon-keranjang.png" style="width: 20px; height: 20px;">
        </a>		
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
  			<form class="form-inline">
    			<input class="form-control mx-auto" type="search" placeholder="Search" aria-label="Search">
    			<button class="btn btn-outline-success mx-auto" type="submit">Search</button>
  			</form>
      	</div>
	</div>

		<div class="nav-item dropdown ml-2 mr-2">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <img src="<?php echo base_url(); ?>/assets/icon-profile.png" style="width: 20px; height: 20px;">
        </a>		
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
  			<a class="nav-link mx-auto" href="<?= base_url(); ?>Home"><p>Logout</p></a>
      	</div>
	</div>
	</div>
</nav>