     <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
 
   <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>

<footer class="test">
    <div class="container mt-5">
    	<div class="row">
    		<div class="col-12">
    			<div class="row text-center">
    				<div class="col-4 mx-auto">
    					<h6>Type</h6>
    					<div class="row">
    						<div class="col">
    							<a href="#" class="text-muted"><small>Eiger</small></a>
    						</div>
    					</div>

    					<div class="row">
    						<div class="col">
    							<a href="#" class="text-muted"><small>Mountain</small></a>
    						</div>
    					</div>

    					<div class="row">
    						<div class="col">
    							<a href="#" class="text-muted"><small>1989</small></a>
    						</div>
    					</div>	
    				</div>
    				<div class="col-4 mx-auto">
    					<h6>About Store</h6>
    					<div class="row">
    						<div class="col">
    							<a href="#" class="text-muted"><small>Tentang Kami</small></a>
    						</div>
    					</div>

    					<div class="row">
    						<div class="col">
    							<a href="#" class="text-muted"><small>Syarat dan ketentuan</small></a>
    						</div>
    					</div>

    					<div class="row">
    						<div class="col">
    							<a href="#" class="text-muted"><small>Info karir</small></a>
    						</div>
    					</div>	
    				</div>
    				<div class="col-4 mx-auto">
    					<h6>Customer Service</h6>
    					<div class="row">
    						<div class="col">
    							<a href="#" class="text-muted"><small>FAQ</small></a>
    						</div>
    					</div>

    					<div class="row">
    						<div class="col">
    							<a href="#" class="text-muted"><small>Cara Pembelian</small></a>
    						</div>
    					</div>

    					<div class="row">
    						<div class="col">
    							<a href="#" class="text-muted"><small>Contact Us</small></a>
    						</div>
    					</div>	
    				</div>    				    				
    			</div>
    		</div>
    	</div>
    </div>
</footer>

<div class="container mx-auto mt-5">
	<div class="row mt-5 text-center">
		<p>Kami menerima metode Pembayaran dengan cara transfer</p>
		<div class="col-10 border-top mx-auto mb-3 mt-4">
			<small>Copyright © 2019 IndoStore.</small>
		</div>
	</div>
</div>
</body>
</html>