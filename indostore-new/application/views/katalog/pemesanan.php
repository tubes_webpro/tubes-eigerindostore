<div class="container-fluid text-center d-flex justify-content-center">
	<div class="row mt-3">
		<div class="col">
			<div class="row">
				<div class="col">
					<div class="card" style="width: 30rem;">
						<h3 class="mt-3">Pemesanan</h3>
					  	<img class="card-img-top" src="<?= base_url() ?>assets/image/<?= $produk->gambar ?>" alt="Card image cap">
					  	<div class="card-body">
					  		<?php  
								$today = date("d/m/Y"); 
								$test = date("Ymd");
								$rand = strtoupper(substr(uniqid(sha1(time())),0,4));
								$unique = $test . $rand;
					  		 ?>
					  		
					  		<form action="" method="post">
					  		<input type="hidden" id="nomor_order" name="nomor_order" value="<?= $unique ?>">
					  		<input type="hidden" id="tanggal_pesan	" name="tanggal_pesan" value="<?= $today ?>">
					  		<input type="hidden" id="userid" name="userid" value="<?= $pengguna->idUser ?>">
					  		<input type="hidden" id="produkid" name="produkid" value="<?= $produk->id_produk ?>">
					  		<input type="hidden" id="alamatid" name="alamatid" value="<?= $alamat->id_alamat ?>">
					  		<input type="hidden" id="verifikasi" name="verifikasi" value='0'>
						  	<h4 class="card-text"><?= $pengguna->namadepan.' '.$pengguna->namabelakang?></h4>
						    <h4 class="card-text"><?= $alamat->alamatJalan?></h4>
						    <h4 class="card-text"><?= $produk->namabarang?></h4>
							<h4 class="card-text"><?= $produk->harga?></h4>
							<h3 class="text-warning">Untuk Pembayaran Silahkan Lakukan menggunakan Transfer bank</h3>
							<div class="row d-flex justify-content-center mt-2 mb-2">
								<div class="col-5" >
									<img src="<?= base_url()?>/assets/gambar/bni.svg" width="150px">
								</div>
								<div class="col-4">
									<h6>sdri.anada</h6>
									<h6>1230812070124s</h6>
								</div>
							</div>
							<div class="row d-flex justify-content-center mt-2 mb-4">
								<div class="col-5">
									<img src="<?= base_url()?>/assets/gambar/bri.png" width="150px">
								</div>
								<div class="col-4">
									<h6>sdri.anada</h6>
									<h6>871097170910</h6>
								</div>
							</div>
						    <div class="row">
					    	<div class="col">
					    		<a href="<?= base_url() ?>Home/detailkatalog/<?= $produk->id_produk ?>">kembali</a>
					    	</div>
					    	<div class="col">
					    		<button type="submit" class="btn btn-primary btn-block"  onclick="return confirm('Apakah anda yakin ingin melakukan pembelian ?')">Tambah Pesanan</button>
					    	</div>
					    </div>
					    </form>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>