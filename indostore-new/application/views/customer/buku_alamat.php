<div class="container">
	<div class="row">
		<div class="col">
			<?php if ($this->session->flashdata('alamat')): ?>    
		        <div class="row">
		            <div class="col-md-8 mx-auto text-center">
		                <div class="alert alert-success alert-dismissible fade show" role="alert">
		                    Alamat <strong> <?= $this->session->flashdata('alamat');  ?></strong>
		                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                    <span aria-hidden="true">&times;</span>
		                    </button>
		                </div>
		            </div>
		        </div>
		    <?php endif ?>
		</div>
	</div>
</div>
<div class="container">
<div class="row">
	<div class="col-3">
		<div class="list-group ">
  			<a href="<?php echo base_url()?>Ccustomer/profile" type="button" class="list-group-item list-group-item-action">Dashboard Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/informasi_akun/<?= $pengguna->idUser?>" type="button" class="list-group-item list-group-item-action">Informasi Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/buku_alamat" type="button" class="list-group-item list-group-item-action active">Buku Alamat</a>
			<a href="<?php echo base_url()?>Ccustomer/pesanan" type="button" class="list-group-item list-group-item-action">Pesanan Saya</a>
			<a href="<?php echo base_url()?>Ccustomer/newslatter" type="button" class="list-group-item list-group-item-action">berlangganan newslatter</a>
			<a href="<?php echo base_url() ?>Ccustomer/ulasan" type="button" class="list-group-item list-group-item-action">Ulasan Produk</a>
			<a href="<?php echo base_url() ?>Ccustomer/wishlist" type="button" class="list-group-item list-group-item-action ">Wishlist</a>					
		</div>
	</div>
	<div class="col">
		<h2>Buku Alamat</h2>
		<div class="bawah bg-primary"></div>
		<div class="row mt-3">
			<div class="col-5">
				<h5>Entri Alamat Tambahan</h5>
				<?php if ($alamat == null): ?>
					<p>Tidak Terdapat Alamat Silahkan Tambahkan alamat</p>
				<?php endif ?>
				<?php if ($alamat != null): ?>
					<p><?php echo $alamat->namadepan." ".$alamat->namabelakang; ?></p>
					<p><?php echo $alamat->alamatJalan ?></p>
					<p><?php echo $alamat->kota." ".$alamat->provinsi." , ".$alamat->kodepos ?></p>
					<p><?php echo $alamat->noTelp ?></p>
				<?php endif ?>
				<div class="row">
					<?php if ($alamat != null): ?>	
					<div class="col-5">
						<a href="<?php echo base_url()?>Ccustomer/edit_alamat/<?= $alamat->idUser?>"><small>Edit Address</small></a>
					</div>
					<div class="col-5">
						<a href="<?php echo base_url()?>Ccustomer/hapus_alamat/<?= $alamat->id_alamat ?>" onclick="return confirm('Apakah Anda yakin ?')"><small>Delete Address</small></a>
					</div>
					<?php endif ?>

				</div>
			</div>
		</div>
                <div class="row mt-3">
                	<?php if ($alamat == null): ?>
                    <div class="col-4 ">
                        <a href="<?= base_url(); ?>Customer/tambah_alamat" class="btn btn-primary btn-block">Tambah Alamat</a>
                    </div>
                	<?php endif ?>
                    <div class="col text-right">
                         <a href="<?= base_url(); ?>Ccustomer/profile"><small class="col-4">kembali</small></a>
                    </div>
                </div>    
	</div>
	</div>
</div>
</div>
</div>