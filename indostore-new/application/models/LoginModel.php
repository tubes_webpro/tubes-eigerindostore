<?php 

class LoginModel extends CI_Model{

	public function login_user($email, $password){
		$this->db->where('email',$email);
		$this->db->where('password',$password);
		$result = $this->db->get('user');
		if($result->num_rows()==1){
			return $result->row(0);
		}else{
			return false;
		}
	}
   
	 public function login_admin($email, $password)
	 {
	  $this->db->where('email', $email);
	  $this->db->where('password',$password);
	  $result = $this->db->get('admin');
		 if($result->num_rows()==1){
			 return $result->row(0);
		 }else{
			 return false;
		 }
	 }
   
	public function register_user($table,$data){
		$data['role'] = 2;
		$insert = $this->db->insert($table, $data);
		if ($insert){
		 return TRUE;
		}else{
		 return FALSE;
		}
	}
	public function tambah($data, $table)
	{
		$input = $this->db->insert($data, $table);
		if($input){
		 return true;
		}
		else{
		 return false;
		}
	}

	public function tambahalamat($data, $table){
		$input = $this->db->insert($table, $data);
		if($input){
		 return true;
		}
		else{
		 return false;
		}
	}

	public function tambahpesanan(){
		$table = "pesanan";
		$data = [
			"nomor_order" => $this->input->post('nomor_order', true),
			"tanggal_pesan" => $this->input->post('tanggal_pesan', true),
			"userid" => $this->input->post('userid', true),
			"produkid" => $this->input->post('produkid', true),
			"alamatid" => $this->input->post('alamatid', true),
			"verifikasi" => $this->input->post('verifikasi', true),
		];
		//use query builder class to update data mahasiswa based on id

		$this->db->insert($table,$data);
	}

	public function getData($table)
	{
	 $result = $this->db->get($table);
	 return $result->result_array();
	}
	
	public function tambahproduk()
	{
	 $gambar = $this->upload->data();
	 $data = [
			"kode" => $this->input->post('kode', true),
			"kategori" => $this->input->post('kategori', true),
			"namabarang" => $this->input->post('namabarang', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"gambar" => $gambar['file_name'],
			"harga" => $this->input->post('harga', true),
		];

		//use query builder class to update data mahasiswa based on id

		$this->db->insert('produk', $data);
	}

	public function ubahproduk()
	{
	$gambar = $this->upload->data();
	 $data = [
			"kode" => $this->input->post('kode', true),
			"kategori" => $this->input->post('kategori', true),
			"namabarang" => $this->input->post('namabarang', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"gambar" => $gambar['file_name'],
			"harga" => $this->input->post('harga', true),
		];
		//use query builder class to update data mahasiswa based on id

		$this->db->where('id_produk', $this->input->post('id'));
		$this->db->update('produk' , $data);
	}

	public function ubahproduk2()
	{
	 $data = [
			"kode" => $this->input->post('kode', true),
			"kategori" => $this->input->post('kategori', true),
			"namabarang" => $this->input->post('namabarang', true),
			"deskripsi" => $this->input->post('deskripsi', true),
			"gambar" => $this->input->post('gambar_lama', true),
			"harga" => $this->input->post('harga', true),
		];
		//use query builder class to update data mahasiswa based on id

		$this->db->where('id_produk', $this->input->post('id'));
		$this->db->update('produk' , $data);
	}
	
	public function ubahDataAlamat()
	{
	 $data = [
			"alamatJalan" => $this->input->post('alamatJalan', true),
			"provinsi" => $this->input->post('provinsi', true),
			"kota" => $this->input->post('kota', true),
			"kodepos" => $this->input->post('kodepos', true),
			"noTelp" => $this->input->post('noTelp', true),
		];
		//use query builder class to update data mahasiswa based on id

		$this->db->where('id_user', $this->input->post('id'));
		$this->db->update('alamat' , $data);
	}

	public function ubahDataAkun()
	{
	 $data = [
			"namadepan" => $this->input->post('namadepan', true),
			"namabelakang" => $this->input->post('namabelakang', true),
			"email" => $this->input->post('email', true),
			"password" => $this->input->post('password', true),
		];
		//use query builder class to update data mahasiswa based on id

		$this->db->where('idUser',$this->input->post('id'));
		$this->db->update('user' , $data);
	}

	public function getAlamatid($id)
	{
		//get data mahasiswa based on id 
		$this->db->where('id_alamat',$id);
		return $this->db->get('alamat')->row_array();
	}

		public function getidUser($id)
	{
		//get data mahasiswa based on id 
		$this->db->where('idUser',$id);
		return $this->db->get('user')->row_array();
	}

		public function getAlamatiduser($id)
	{
		//get data mahasiswa based on id 
		$this->db->where('id_user',$id);
		return $this->db->get('alamat')->row_array();
	}

	public function hapusalamat($id)
	{
		//use query builder to delete data based on id 
		return $this->db->delete('alamat', array("id_alamat"=>$id));
	}

	public function hapusproduk($id)
	{
		//use query builder to delete data based on id 
		return $this->db->delete('produk', array("id_produk"=>$id));
	}
	
	public function tampilData1()
	{
		return $this->db->where('kategori', "mountaineering")->get('produk');
	}

	public function tampilData2()
	{
		return $this->db->where('kategori', "1989")->get('produk');
	}

	public function tampilData3()
	{
		return $this->db->where('kategori', "riding")->get('produk');
	}

	public function tampilproduk()
	{
		return $this->db->get('produk');
	}

	public function getidproduk($id)
	{
		//get data mahasiswa based on id 
		$this->db->where('id_produk',$id);
		return $this->db->get('produk')->row_array();
	}
	public function verifikasi($id)
	{
		$data = [
			"verifikasi" => 1,
		];
		//use query builder class to update data mahasiswa based on id
		$this->db->where('id_pesanan',$id);
		$this->db->update('pesanan' , $data);
	}
}

 ?>