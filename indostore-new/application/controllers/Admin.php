<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  admin extends CI_Controller {

	public function index()
	{
		$data['judul'] = 'indoStore';
		$data['pengguna'] = $this->db->get_where('user', ['idUser' => $this->session->userdata('idUser')])->row(1);
		$data['barang'] = $this->LoginModel->tampilproduk()->result();
		$this->load->view('template/header2', $data);
		$this->load->view('Admin/halamanadmin',$data);
		$this->load->view('template/footer');	
	}


	public function tambah_barang()
	{
		$data['judul'] = 'indoStore';	
		$this->form_validation->set_rules('kode', 'Kode', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('namabarang', 'Namabarang', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		$this->form_validation->set_rules('harga', 'Harga', 'required');

		$this->load->library('upload');
		$config['upload_path'] = './assets/image';
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']     = '2048';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$nama_file = "gambar_".time();
		$config['file_name'] = $nama_file;

		
		$field_name = "gambar";
		// Alternately you can set preferences by calling the ``initialize()`` method. Useful if you auto-load the class:
		$this->upload->initialize($config);
		if($this->form_validation->run() && $this->upload->do_upload($field_name) ){
			if ($_FILES['gambar']['name']) {
				
				$this->LoginModel->tambahproduk();
				$this->session->set_flashdata('flash','Data Berhasil Diubah');
				redirect('admin');
			}
		}else{
			$this->load->view('template/header2', $data);
			$this->load->view('Admin/tambah_barang');
			$this->load->view('template/footer');	
		}
	}


	public function hapusproduk($id)
	{
		//call method hapusDataMahasiswa with parameter id from mahasiswa_model
		//use flashdata to show alert "dihapus"
		//back to controller mahasiswa
		$this->LoginModel->hapusproduk($id);
		$this->session->set_flashdata('flash', 'Data berhasil Dihapus');
		redirect('Admin');
	}

	public function editproduk($id)
	{
		$data['judul'] = 'indoStore';	
		$data['produk'] = $this->LoginModel->getidproduk($id);
		$this->form_validation->set_rules('kode', 'Kode', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('namabarang', 'Namabarang', 'required');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
		$this->form_validation->set_rules('harga', 'Harga', 'required');

		$this->load->library('upload');
		$path = './assets/image';
		$config['upload_path'] = $path;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']     = '2048';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$nama_file = "gambar_".time();
		$config['file_name'] = $nama_file;

		
		$field_name = "gambar";
		// Alternately you can set preferences by calling the ``initialize()`` method. Useful if you auto-load the class:
		$this->upload->initialize($config);
		$id_produk = $this->input->post('id_produk');
		$gambar_lama = $this->input->post('ganti_gambar');
		if($this->form_validation->run()){
			if ($this->upload->do_upload($field_name)) {
				if ($_FILES['gambar']['name']) {	
					@unlink($path, $gambar);
					$this->LoginModel->ubahproduk();
					$this->session->set_flashdata('flash','Data Berhasil Diubah');
					redirect('admin');
				}
			}
			elseif (empty($_FILES['gambar']['name'])) {
				$this->LoginModel->ubahproduk2();
				$this->session->set_flashdata('flash','Data Berhasil Diubah');
				redirect('admin');
			}
		}else{
			$this->load->view('template/header2', $data);
			$this->load->view('Admin/editproduk',$data);
			$this->load->view('template/footer');	
		}
	}

	public function lihat_pesanan()
	{
		$data['judul'] = 'indoStore';
		$this->db->select('*');
		$this->db->from('pesanan');
		$this->db->join('alamat', 'pesanan.alamatid = alamat.id_alamat');
		$this->db->join('produk','pesanan.produkid = produk.id_produk');
		$this->db->join('user','pesanan.userid = user.idUser');
		$pes = $this->db->get();
		$data['pesanan'] = $pes->result();
		$this->load->view('template/header2', $data);
		$this->load->view('Admin/lihat_pesanan',$data);
		$this->load->view('template/footer');
	}

	public function verifikasi($id)
	{
		$this->LoginModel->verifikasi($id);
		$this->session->set_flashdata('flash', 'Data Terverifikasi');
		redirect('Admin/lihat_pesanan');
	}
}

/* End of file admin */
/* Location: ./application/controllers/admin */