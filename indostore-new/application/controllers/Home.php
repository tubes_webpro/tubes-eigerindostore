<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Home extends CI_Controller {
		function __construct(){
		parent::__construct();
		    $this->load->model('LoginModel');
		    $this->load->helper('date');
		}

		public function index()
		{
			$data['judul'] = 'indoStore';
			$this->load->view('template/header2', $data);
			$this->load->view('home');
			$this->load->view('template/footer');		
		}

		public function katalog()
		{
			$data['judul'] = 'indoStore';
			$data['tes'] = $this->LoginModel->tampilData1()->result();
			$this->load->view('template/header2', $data);
			$this->load->view('katalog/katalog',$data);
			$this->load->view('template/footer');	
		}

		public function katalog1()
		{
			$data['judul'] = 'indoStore';
			$data['tes'] = $this->LoginModel->tampilData2()->result();
			$this->load->view('template/header2', $data);
			$this->load->view('katalog/katalog1',$data);
			$this->load->view('template/footer');
		}

		public function katalog2()
		{
			$data['judul'] = 'indoStore';
			$data['tes'] = $this->LoginModel->tampilData3()->result();
			$this->load->view('template/header2', $data);
			$this->load->view('katalog/katalog2',$data);
			$this->load->view('template/footer');
		}

		public function detailkatalog($id)
		{
			$data['judul'] = 'indoStore';
			$data['produk'] = $this->db->get_where('produk', ['id_produk' => $id])->row(1);
			$this->load->view('template/header2', $data);
			$this->load->view('katalog/katalogdetail',$data);
			$this->load->view('template/footer');	
		}

		public function pesanan($id)
		{
			$data['judul'] = 'indoStore';	
			$data['pengguna'] = $this->db->get_where('user', ['idUser' => $this->session->userdata('idUser')])->row(1);
			$data['alamat'] = $this->db->get_where('alamat', ['id_user' =>  $this->session->userdata('idUser')])->row(1);
			$data['produk'] = $this->db->get_where('produk', ['id_produk' => $id])->row(1);

			$this->form_validation->set_rules('userid', 'Userid', 'required');
			$this->form_validation->set_rules('produkid', 'Produkid', 'required');
			$this->form_validation->set_rules('alamatid', 'Alamatid', 'required');

			if($this->form_validation->run()==FALSE){
				if (isset($_SESSION['role'])) {
				$this->load->view('template/header2', $data);
				$this->load->view('katalog/pemesanan',$data);
				$this->load->view('template/footer');
				}else
				{
					redirect('Ccustomer');
				}
			}else{
				$this->LoginModel->tambahpesanan();
				$this->session->set_flashdata('Pesanan','Berhasil Diubah');
				redirect('Ccustomer/profile');
			}
		}
}
?>