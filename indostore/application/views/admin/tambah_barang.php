<div class="container mt-3"> 
		<div class="row"> 
				<div class="col">
                                    <h5>Informasi Pribadi</h5>
                                    <p class="border-bottom mb-4 mt-2"></p>
                                </div>
                            </div>
                            <form action="<?php echo site_url().'/Ccustomer/tambah_barang'; ?>" method="POST">
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Kode Barang</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="kdbarang" name="kdbarang" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Nama Barang</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="namabarang" name="namabarang" required>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Deskripsi</label>
                                </div>
                                <div class="col">
                                    <textarea type="text" class="form-control mb-4" id="deskripsi" name="deskripsi" required></textarea>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Gambar</label>
                                </div>
                                <div class="col">
                                    <input type="file" id="gambar" name="gambar" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Harga</label>
                                </div>
                                <div class="col">
                                    <input type="number" min="0" max="10000000" class="form-control mb-4" id="harga" name="harga" value="0" required>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-5">
                                    <button type="submit" name="masuk" class="btn btn-primary btn-block">Submit</button>
                                </div>
                                <a href="<?= base_url(); ?>Ccustomer/admin"><small class="col-4">kembali</small></a>
                            </div>
                            </form>
                        </div>
		</div>			
</div>	