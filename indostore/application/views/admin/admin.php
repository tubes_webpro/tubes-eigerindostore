<div class="container"> 
<div class="row text-center"> 
		<div class="col mt-3"> 
				<h1>Upload Barang</h1>
				<div class="row d-flex justify-content-center"> 
						<div class="col"> 
								<table class="table">
								  <thead>
								    <tr>
								      <th scope="col">#</th>
								      <th scope="col">kode Barang</th>
								      <th scope="col">Nama Barang</th>
									 	<th scope="col">deskripsi</th>
									 	<th scope="col">gambar</th>
								      <th scope="col">Harga Barang</th>
								      <th scope="col">Opsi</th>
								    </tr>
								  </thead>
								  <tbody>
								  	<?php foreach ($keadmin as $key): 
								  		# code...
								  	 ?>
								    <tr>
								      <th scope="row">1</th>
								      <td><?php echo $key['kode']; ?></td>
								      <td><?php echo $key['namabarang']; ?></td>
											<td><?php echo $key['deskripsi']; ?></td>
											<td><img src="<?php echo $key['gambar']; ?>" alt=""></td>
								      <td><?php echo $key['harga']; ?></td>
								      <td><a href="">edit</a>  |  <a href="">Hapus</a></td>
									    </tr>
									    <tr>
									<?php endforeach; ?>    	<!-- sampai sini -->
								  </tbody>
								</table>
						</div>	
				</div>
				<div class="row"> 
						<a href="<?php echo base_url() ?>admin/tambah_barang" class="btn btn-primary ml-2">Tambahkan Barang</a>
				</div>	
		</div>	
</div>		
</div>