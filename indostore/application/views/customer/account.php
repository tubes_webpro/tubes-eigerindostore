<div class="container mb-4 mt-5">
    <div class="row">
        <div class="col-8 mx-auto">
            <h1 class="mb-2">Login Pelanggan</h1>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-8 mx-auto">
            <div class="row">
                <div class="col-md-6">
                        <div class="content">
                            <h5 >Pelanggan Terdaftar</h5>
                            <p class="border-bottom mb-2 mt-2"></p>
                            <p>Jika kamu sudah punya akun, silakan login menggunakan email.</p>
                            <form action="<?php echo site_url().'Ccustomer/login'; ?>" method="post">
                                <div class="form-group row">
                                    <label for="email" class="col-form-label col-4">E-mail *</label>
                                    <div class="col">
                                        <input type="text" class="form-control" id="email" name="email" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Password" class="col-form-label col-4">Password</label>
                                    <div class="col">
                                        <input type="password" class="form-control" id="email" name="password" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4"></div>
                                    <div class="col">
                                        <button type="submit" name="masuk" class="btn btn-primary btn-block">Masuk</button>
                                        <a href="#"><small>Lupa password?</small></a>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
                <div class="col-md-6">
                    <div class="item h-100 d-flex flex-column justify-content-between">
                        <div class="content">
                            <h5>Pelanggan Baru</h5>
                            <p class="border-bottom mb-2 mt-2"></p>
                            <p>Dengan membuat akun Anda dapat: berbelanja lebih cepat, menyimpan lebih dari satu alamat, melacak pesanan dan lainnya.</p>
                        </div>
                        <div class="row">
                            <div class="col-8">
                                <a href="<?= site_url(); ?>customer/regis" class="btn btn-primary btn-block">Buat Akun</a>
                                <small style="visibility: hidden">aksjakjsk</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>