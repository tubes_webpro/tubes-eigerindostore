<div class="container">
    <div class="row">
        <div class="col-12 mx-auto">
            <h2 class="mb-4">Buat Akun Pelanggan Baru</h2>
        </div>
    </div>
</div>

<div class="container">
    <form action="<?php echo site_url().'/Ccustomer/regis';?>" method="post">
        <div class="row">        
            <div class="col-12 mx-auto">
                <div class="row">
                    <div class="col-6">
                        <div class="content">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col">
                                    <h5>Informasi Pribadi</h5>
                                    <p class="border-bottom mb-4 mt-2"></p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Nama Depan</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="namadepan" name="namadepan" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Nama Belakang</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="namabelakang" name="namabelakang" required>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col">
                                    <h5>Informasi Sign-In</h5>
                                    <p class="border-bottom mb-4 mt-2"></p>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Email</label>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control mb-4" id="email" name="email" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Password</label>
                                </div>
                                <div class="col">
                                    <input type="password" class="form-control mb-4" id="password" name="password" required>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-3">
                                    <label for="nama">Konfirmasi Password</label>
                                </div>
                                <div class="col">
                                    <input type="password" class="form-control mb-4" id="kpassword" name="kpassword" required>
                                </div>
                            </div>
                        
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-5">
                                    <button type="submit" name="masuk" class="btn btn-primary btn-block">Buat Akun</button>
                                </div>
                                <a href="<?= base_url(); ?>Ccustomer"><small class="col-4">kembali</small></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>