<div class="container">
<div class="row">
	<div class="col-3">
		<div class="list-group ">
  			<a href="<?php echo base_url()?>Ccustomer/profile" type="button" class="list-group-item list-group-item-action">Dashboard Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/informasi_akun" type="button" class="list-group-item list-group-item-action">Informasi Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/buku_alamat" type="button" class="list-group-item list-group-item-action">Buku Alamat</a>
			<a href="<?php echo base_url()?>Ccustomer/pesanan" type="button" class="list-group-item list-group-item-action active">Pesanan Saya</a>
			<a href="<?php echo base_url()?>Ccustomer/newslatter" type="button" class="list-group-item list-group-item-action">berlangganan newslatter</a>
			<a href="<?php echo base_url() ?>Ccustomer/ulasan" type="button" class="list-group-item list-group-item-action">Ulasan Produk</a>
			<a href="<?php echo base_url() ?>Ccustomer/wishlist" type="button" class="list-group-item list-group-item-action ">Wishlist</a>			
		</div>
	</div>
	<div class="col">
		<h2>Pesanan Saya</h2>
		<div class="bawah bg-primary"></div>
		<div class="row mt-3 ">
			<div class="col mt-3">

                <!-- jika data kosong tampil kan ini

                <div class="row">
                    <div class="col">
                        <p class="bg-warning pd-1">Anda belum pernah membuat pesanan.</p>
                    </div>
                </div>

                sampai disini -->


                <!-- jika data ada tampilkan ini -->

                <div class="row border-bottom">
                    <div class="col-2">
                        <p>Order #</p>
                    </div>
                    <div class="col-2">
                        <p>Date</p>
                    </div>
                    <div class="col-2">
                        <p>Ship To</p>
                    </div>
                    <div class="col-2">
                        <p>Total</p>
                    </div>
                    <div class="col-2">
                        <p>Status</p>
                    </div>
                </div>

                <!-- foreach disini -->
                <div class="row mt-2 border-bottom">
                    <div class="col-2">
                        <p>2000029185</p>
                    </div>
                    <div class="col-2">
                        <p>20/04/19</p>
                    </div>
                    <div class="col-2">
                        <p>chalik fachruddin</p>
                    </div>
                    <div class="col-2">
                        <p> IDR 55.000,00</p>
                    </div>
                    <div class="col-2">
                        <p>Pending</p>
                    </div>
                    <div class="col-2">
                        <a href="<?php echo base_url()?>#">Lihat Pesanan </a>
                    </div>
                </div>
                <!-- sampai disini -->

            </div>
		</div>
        
        <div class="row mt-3">
            <div class="col-1">
                <p>1 item</p>
            </div>
        </div>

        <div class="row mt-5">
                        <div class="col text-right">
                <a href="<?= base_url(); ?>Ccustomer/profile"><small class="col-4">kembali</small></a>
            </div>
        </div>
		</div>		
	</div>
</div>
</div>
</div>