<div class="container">
<div class="row">
	<div class="col-3">
		<div class="list-group">
  			<a href="<?php echo base_url()?>Ccustomer/profile" type="button" class="list-group-item list-group-item-action">Dashboard Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/informasi_akun" type="button" class="list-group-item list-group-item-action active ">Informasi Akun</a>
			<a href="<?php echo base_url()?>/Ccustomer/buku_alamat" type="button" class="list-group-item list-group-item-action">Buku Alamat</a>
			<a href="<?php echo base_url()?>/Ccustomer/pesanan" type="button" class="list-group-item list-group-item-action">Pesanan Saya</a>
			<a href="<?php echo base_url()?>Ccustomer/newslatter" type="button" class="list-group-item list-group-item-action">berlangganan newslatter</a>
			<a href="<?php echo base_url() ?>Ccustomer/ulasan" type="button" class="list-group-item list-group-item-action">Ulasan Produk</a>
			<a href="<?php echo base_url() ?>Ccustomer/wishlist" type="button" class="list-group-item list-group-item-action ">Wishlist</a>					
		</div>
	</div>
	<div class="col mt-3">
		<h2>Edit informasi Akun</h2>
		<div class="bawah bg-primary"></div>
		<div class="row mt-3 ">
			<div class="col-6">
				<h4 class="border-bottom">Informasi Akun</h4>
			</div>
		</div>
		<form action="<?php echo site_url().'/Ccustomer/informasi_akun' ?>" method="POST">
			<div class="row mt-4">
				<div class="col">
					<p>Nama Depan</p>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<input type="text" class="form-control mb-4" id="namadepan" name="namadepan" placeholder="<?php $data = $infoAkun; echo $data->namadepan; ?>">
				</div>
			</div>
			<div class="row mt-3">
				<div class="col">
					<p>Nama Belakang</p>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<input type="text" class="form-control mb-4" id="namabelakang" name="namabelakang" placeholder="<?php echo $data->namabelakang; ?>">
				</div>
			</div>
			<div class="row">
				<div class="col-3">
					<button type="submit" name="masuk1" class="btn btn-primary btn-block">SIMPAN</button>
				</div>
			</div>
		</form>
		<div class="row-3"></div>
		<div class="row mt-3 ">
			<div class="col-6">
				<h4 class="border-bottom">Ubah Email</h4>
			</div>
		</div>
		<div class="row mt-4">
			<div class="col">
				<p>Email</p>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<input type="text" class="form-control mb-4" id="email" name="email" placeholder="<?php echo $data->email; ?>">
			</div>
		</div>
		<div class="row mt-3">
			<div class="col">
				<p>Password Saat ini</p>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<input type="password" class="form-control mb-4" id="password" name="password">
			</div>
		</div>
		<div class="row">
			<div class="col-3">
				<button type="submit" name="masuk1" class="btn btn-primary btn-block">SIMPAN</button>
			</div>
		</div>
		<div class="row-3"></div>
		<div class="row mt-3 ">
			<div class="col-6">
				<h4 class="border-bottom">Ubah Email</h4>
			</div>
		</div>
		<div class="row mt-3">
			<div class="col">
				<p>Password Saat ini</p>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<input type="password" class="form-control mb-4" id="password" name="password">
			</div>
		</div>
		<div class="row mt-3">
			<div class="col">
				<p>Password Baru</p>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<input type="password" class="form-control mb-4" id="password1" name="password1">
			</div>
		</div>
		<div class="row mt-3">
			<div class="col">
				<p>Ulangi Password Baru</p>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<input type="password" class="form-control mb-4" id="password2" name="password2">
			</div>
		</div>
		<div class="row">
			<div class="col-3">
				<button type="submit" name="masuk1" class="btn btn-primary btn-block">SIMPAN</button>
			</div>
			<div class="col ml-5 text-right">
				<small><a href="<?php echo base_url()?>Ccustomer/profile">kembali</a></small>
			</div>
		</div>
	</div>
</div>
</div>
</div>