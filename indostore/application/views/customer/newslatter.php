<div class="container">
<div class="row">
	<div class="col-3">
		<div class="list-group ">
  			<a href="<?php echo base_url()?>Ccustomer/profile" type="button" class="list-group-item list-group-item-action">Dashboard Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/informasi_akun" type="button" class="list-group-item list-group-item-action">Informasi Akun</a>
			<a href="<?php echo base_url()?>/Ccustomer/buku_alamat" type="button" class="list-group-item list-group-item-action">Buku Alamat</a>
			<a href="<?php echo base_url()?>/Ccustomer/pesanan" type="button" class="list-group-item list-group-item-action">Pesanan Saya</a>
			<a href="<?php echo base_url()?>Ccustomer/newslatter" type="button" class="list-group-item list-group-item-action active">berlangganan newslatter</a>
			<a href="<?php echo base_url() ?>Ccustomer/ulasan" type="button" class="list-group-item list-group-item-action">Ulasan Produk</a>
			<a href="<?php echo base_url() ?>Ccustomer/wishlist" type="button" class="list-group-item list-group-item-action ">Wishlist</a>				
		</div>
	</div>
	<div class="col">
		<h2>Berlangganan Newslatter</h2>
		<div class="bawah bg-primary"></div>
		<div class="row mt-3">
			<div class="col">
				<div class="row">
					<div class="col">
						<p class="border-bottom">Subscription option</p>
					</div>
				</div>
				<div class="row mt-3 mb-3">
					<div class="col-1 mt-3">
						<input type="checkbox" name="subs">
					</div>
					<div class="col-2 text-center">
						<h4>Mulai berlangganan</h4>
					</div>
				</div>
	            <div class="row mt-3">
	                <div class="col-4 ">
	                    <a href="<?= base_url(); ?>Ccustomer/tambah_alamat" class="btn btn-primary btn-block">Tambah Alamat</a>
	                </div>
	                <div class="col text-right">
	                	<a href="<?= base_url(); ?>Ccustomer/profile"><small class="col-4">kembali</small></a>
	                </div>
	            </div>    
			</div>
	</div>
	</div>
</div>
</div>
</div>