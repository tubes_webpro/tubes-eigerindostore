<div class="container">
<div class="row">
	<div class="col-3">
		<div class="list-group ">
  			<a href="<?php echo base_url()?>Ccustomer/profile" type="button" class="list-group-item list-group-item-action">Dashboard Akun</a>
			<a href="<?php echo base_url()?>Ccustomer/informasi_akun" type="button" class="list-group-item list-group-item-action">Informasi Akun</a>
			<a href="<?php echo base_url()?>/Ccustomer/buku_alamat" type="button" class="list-group-item list-group-item-action active">Buku Alamat</a>
			<a href="<?php echo base_url()?>/Ccustomer/pesanan" type="button" class="list-group-item list-group-item-action">Pesanan Saya</a>
            <a href="<?php echo base_url()?>Ccustomer/newslatter" type="button" class="list-group-item list-group-item-action">berlangganan newslatter</a>
			<a href="<?php echo base_url() ?>Ccustomer/ulasan" type="button" class="list-group-item list-group-item-action">Ulasan Produk</a>
			<a href="<?php echo base_url() ?>Ccustomer/wishlist" type="button" class="list-group-item list-group-item-action ">Wishlist</a>      		
		</div>
	</div>
<div class="col">
<h2>Tambah Alamat Baru</h2>
<div class="bawah bg-primary"></div>
<div class="row mt-3 ">
    <div class="col-2"></div>
    <div class="col mt-3">
        <form action="<?php echo site_url().'Ccustomer/tambah_alamat'; ?>" method="POST"> 
                        <div class="row">
                            <div class="col-3">
                                <label for="nama">No Telpon</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control mb-4" id="namadepan" name="telp" required>
                            </div>
                        </div>  

                        <div class="row">
                            <div class="col-3">
                                <label for="nama">Alamat jalan</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control mb-4" id="namadepan" name="alamat" required>
                            </div>
                        </div>  

                        <div class="row">
                            <div class="col-3">
                                <label for="nama">Provinsi</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control mb-4" id="provinsi" name="provinsi" required>
                            </div>
                        </div>  

                        <div class="row">
                            <div class="col-3">
                                <label for="nama">Kota</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control mb-4" id="kota" name="kota" required>
                            </div>
                        </div>  

                        <div class="row">
                            <div class="col-3">
                                <label for="nama">Kode Pos</label>
                            </div>
                            <div class="col">
                                <input type="text" class="form-control mb-4" id="namadepan" name="pos" required>
                            </div>
                        </div>       

                        <div class="row">
                            <div class="col-4">
                                <button type="submit" name="masuk" class="btn btn-primary btn-block">Simpan Alamat</button>
                            </div>
                            <div class="col text-right">
                                 <a href="<?= base_url(); ?>Ccustomer/buku_alamat"><small class="col-4">kembali</small></a>
                            </div>
                        </div>      
        </form>
    </div>
</div>
</div>

	</div>
</div>
</div>
</div>