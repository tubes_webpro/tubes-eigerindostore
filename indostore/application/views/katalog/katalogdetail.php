<div class="container-fluid">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-1">
			<a href="" class="text-muted"><h6>MEN</h6></a>
		</div>
		<div class="col-1">
			<a href="" class="text-muted"><h6>WOMEN</h6></a>
		</div>
		<div class="col-1">
			<a href="" class="text-muted"><h6>JUNIOR</h6></a>
		</div>
		<div class="col-2">
			<a href="" class="text-muted"><h6>TOOL & EQUIPMENT</h6></a>
		</div>
		<div class="col-2">
			<a href="" class="text-muted"><h6>CLIMBING EQUIPMENT</h6></a>
		</div>
		<div class="col-1">
			<a href="" class="text-muted"><h6>ACCESSORIES</h6></a>
		</div>
	</div>
	<div class="row">
		<div class="col ml-4">
			<nav aria-label="breadcrumb">
	  			<ol class="breadcrumb bg-white">
	   				<li class="breadcrumb-item"><a href="#">Home</a></li>
	    			<li class="breadcrumb-item" aria-current="page">Mountaineering</li>
					<li class="breadcrumb-item active" aria-current="page">Produk</li>	    			
	  			</ol>
			</nav>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">
			<div class="col-6 text-right">
				<img src="<?php echo base_url() ?>assets/gambar/1.jpg" style="height: 500px; width: 500px;">
			</div>

			<div class="col-6 text-left">
					<h1>Nama Produk</h1>
					<h4>IDR. <?php echo "565.000" ?></h4> <!-- isinya harga dari database -->
					<div class="btn-group" role="group" aria-label="Basic example">
					  <button type="button" class="btn btn-secondary">S</button>
					  <button type="button" class="btn btn-secondary">M</button>
					  <button type="button" class="btn btn-secondary">L</button>
					  <button type="button" class="btn btn-secondary">XL</button>
					</div>
					<div class="a row mt-2">
						<div class="col">
							<input type="number" min="1" max="5" value="1">
						</div>
						<div class="col">
							<button class="btn btn-primary">TAMBAH KE KERANJANG</button>
						</div>
					</div>
					
					<div class="border-bottom mt-3"></div>
					
					<div class="row mt-3">
						<div class="col">
							<button class="btn btn-primary">TAMBAHKAN KE WISHLIST</button>
						</div>
					</div>

					<div class="row mt-1">
						<div class="col">
							<p>
								Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
							</p>
						</div>
					</div>
			</div>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-6">
					<h3>RINCIAN</h3>
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</p>
				</div>

				<div class="col-6">
					<h3>PRODUCT SPESIFICATION</h3>
					<div class="row">
						<div class="col-4">
							<h6>GENDER</h6>
						</div>
						<div class="col">
							<h6><?php echo "Man" ?></h6>
						</div>
					</div>

					<div class="row">
						<div class="col-4">
							<h6>MEREK</h6>
						</div>
						<div class="col">
							<h6><?php echo "EIGER" ?></h6>
						</div>
					</div>

					<div class="row">
						<div class="col-4">
							<h6>WARNA</h6>
						</div>
						<div class="col">
							<h6><?php echo "blue" ?></h6>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="container mt-5">
			<div class="row">
				<div class="col-3 ml-5">
					<h4>ULASAN WRITE YOUR OWN REVIEW</h4>
				</div>
				<div class="col">
					<div class="row">
						<div class="col"></div>
					</div>
					<form action="" method="POST">
					<div class="row">
						<div class="col-4">
							<input class="col-10" type="text" name="nama" id="nama" placeholder="NAMA ANDA">
						</div>
						<div class="col-5">
							<input class="col-10" type="text" name="nama" id="nama" placeholder="JUDUL ULASAN">
						</div>
					</div>
					<div class="row mt-2">
						<div class="col">
							<textarea name="ulasan" id="ulasan" rows="10" cols="68">
							</textarea>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col-3">
							<button type="submit" name="ulasan" class="btn btn-primary btn-block">Simpan Ulasan</button>
						</div>
					</div>
				</form>
				</div>
			</div>
		</div>
	</div>

</div>