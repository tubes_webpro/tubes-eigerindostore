<div class="container-fluid">
	<div class="row mt-4">
		<div class="col-3"></div>
		<div class="col-1">
			<a href="" class="text-muted"><h6>MEN</h6></a>
		</div>
		<div class="col-1">
			<a href="" class="text-muted"><h6>WOMEN</h6></a>
		</div>
		<div class="col-1">
			<a href="" class="text-muted"><h6>JUNIOR</h6></a>
		</div>
		<div class="col-2">
			<a href="" class="text-muted"><h6>TOOL & EQUIPMENT</h6></a>
		</div>
		<div class="col-2">
			<a href="" class="text-muted"><h6>CLIMBING EQUIPMENT</h6></a>
		</div>
		<div class="col-1">
			<a href="" class="text-muted"><h6>ACCESSORIES</h6></a>
		</div>
	</div>
	<div class="row">
		<div class="col ml-4">
			<nav aria-label="breadcrumb">
	  			<ol class="breadcrumb bg-white">
	   				<li class="breadcrumb-item"><a href="#">Home</a></li>
	    			<li class="breadcrumb-item active" aria-current="page">Mountaineering</li>
	  			</ol>
			</nav>
		</div>
	</div>


	<div class="row">
		<div class="col-2 ml-5">
			<h4>FILTERS</h4>
		</div>
		<div class="col">
			<p>Produk <?php echo "1 - 9" ?> dari <?php echo "9"; ?></p>
		</div>
		<div class="col text-right mr-5">
			<p>Urutkan Berdasarkan</p>
			<div class="row">
				<div class="col">                           
                    <select id="urutkan" name="urutkan">
                        <option value="Posisi">-</option>
	                    <option value="nama_produk">Nama Produk</option>
	                    <option value="Harga">Harga</option>
	                    <option value="Top_rated">Top Rated</option>
                   	</select>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-3">
			<div class="row">
				<div class="col ml-5">
					<h5>KATEGORI</h5>
				</div>
			</div>

			<div class="row">
				<div class="col">

					<div class="row">
						<div class="col-1">
							<input type="checkbox" name="men" value="men">
						</div>
						<div class="col">
							<h6>MEN</h6>
						</div>
					</div>

					<div class="row">
						<div class="col-1">
							<input type="checkbox" name="women" value="women">
						</div>
						<div class="col">
							<h6>WOMEN</h6>
						</div>
					</div>

					<div class="row">
						<div class="col-1">
							<input type="checkbox" name="junior" value="junior">
						</div>
						<div class="col">
							<h6>JUNIOR</h6>
						</div>
					</div>

					<div class="row">
						<div class="col-1">
							<input type="checkbox" name="tool&equipment" value="tool&equipment">
						</div>
						<div class="col">
							<h6>TOOL & EQUIPMENT</h6>
						</div>
					</div>

					<div class="row">
						<div class="col-1">
							<input type="checkbox" name="climbing" value="climbing">
						</div>
						<div class="col">
							<h6>CLIMBING EQUIPMENT</h6>
						</div>
					</div>

					<div class="row">
						<div class="col-1">
							<input type="checkbox" name="accessories" value="accessories">
						</div>
						<div class="col">
							<h6>ACCESSORIES</h6>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col">
	        <div class="row text-center">
				<!-- foreach disini -->
	            <div class="col-4">
	            	<a href="<?php echo base_url() ?>Home/detailkatalog">
		                <img src="<?php echo base_url() ?>assets/gambar/1.jpg" style="width: 200px; height: 200px;">
		            </a>    
		                <p>Nama Produk</p>
		                <p>Harga Produk</p>
		                <div class="row"></div>
	        	</div>        	
				<!-- sampai sini -->
			</div>
		</div>
	</div>
</div>