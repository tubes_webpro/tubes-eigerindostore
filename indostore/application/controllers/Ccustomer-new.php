<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ccustomer extends CI_Controller {

	function __construct(){
	    parent::__construct();
	    $this->load->model('LoginModel');
	}

	public function index()
	{
		$data['judul'] = 'indoStore';
		$this->load->view('template/header', $data);
		$this->load->view('customer/account');
		$this->load->view('template/footer');
	}

	public function admin()
	{
		$data['judul'] = 'indoStore';
		$table = 'produk';
		$ambil = $this->LoginModel->getData($table);
		$data1['keadmin'] = $ambil;
		$this->load->view('template/header2', $data);
		$this->load->view('admin/admin', $data1);
		$this->load->view('template/footer');
	}		


	public function tambah_barang()
	{
		$data['judul'] = 'indoStore';

		$kode = $this->input->post('kdbarang');
		$nama = $this->input->post('namabarang');
		$deskripsi = $this->input->post('deskripsi');
		$gambar = $this->input->post('gambar');
		$harga = $this->input->post('harga');

		$data = array(
			'kode' => $kode,
			'namabarang' => $nama,
			'deskripsi' =>$deskripsi,
			'gambar' => $gambar,
			'harga' => $harga
		);
		$table = 'produk';
		$add = $this->LoginModel->tambahproduk($data, $table);
		if($add){
			$this->session->set_flashdata('alert', 'sukses_insert');
		}
		else{
			echo "<script>alert('Gagal Menambahkan Data');</script>";
		}
		$this->load->view('template/header2', $data);
		$this->load->view('admin/tambah_barang');
		$this->load->view('template/footer');
	}	

	public function profile()
	{
		$data['judul'] = 'indoStore';
		
		$email = $this->session->userdata('email');
		$table = 'user';

		$akun = $this->LoginModel->getByEmail($email, $table);
		$data1['infoAkun'] = $akun;

		$this->load->view('template/header2', $data);
		$this->load->view('customer/profile', $data1);
		$this->load->view('template/footer');
	}

	public function informasi_akun()
	{
		$data['judul'] = 'indoStore';
		/*get buat placeholder*/
		
		$email = $this->session->userdata('email');
		$table = 'user';

		$akun = $this->LoginModel->getByEmail($email, $table);
		
		$data1['infoAkun'] = $akun;
		/*edit form pertama*/
		$namad = $this->input->post('namadepan');
		$namab = $this->input->post('namabelakang');
		$apaja = array(

			'namadepan' => $namad,
			'namabelakang'=> $namab
		);
		$update = $this->LoginModel->editDataByEmail($email, $apaja, 'user');
		$this->load->view('template/header2', $data);	
		$this->load->view('customer/informasi_akun', $data1);
		$this->load->view('template/footer');
	}

	public function buku_alamat()
	{
		$data['judul'] = 'indoStore';
		$this->load->view('template/header2', $data);	
		$this->load->view('customer/buku_alamat');
		$this->load->view('template/footer');
	}

	public function edit_alamat()
	{
		$data['judul'] = 'indoStore';

		$email = $this->session->userdata('email');
		$table = 'alamat';
		$akun = $this->LoginModel->getByEmail($data, $table);

		$this->load->view('template/header2', $data);	
		$this->load->view('customer/edit_alamat');
		$this->load->view('template/footer');
	}

	public function tambah_alamat()
	{
		$data['judul'] = 'indoStore';
		/*$email = $this->input->post('email');*/
		$notelp = $this->input->post('telp');
		$alamat = $this->input->post('alamat');
		$provinsi = $this->input->post('provinsi');
		$kota = $this->input->post('kota');
		$kodepos = $this->input->post('pos');

		$data = array(
			'email' => $this->session->userdata('email'),
			'alamatJalan' => $alamat,
			'provinsi' => $provinsi, 
			'kota' => $kota,
			'kodepos' => $kodepos,
			'noTelp' => $notelp
		);
		$table = 'alamat';
		$add = $this->LoginModel->tambahalamat($data, $table);
		if($add){
			$this->session->set_flashdata('alert', 'sukses_insert');
		}
		else{
			echo "<script>alert('Gagal Menambahkan Data');</script>";
		}

		$this->load->view('template/header2', $data);	
		$this->load->view('customer/tambah_alamat');
		$this->load->view('template/footer');


	}

	public function pesanan()
	{
		$data['judul'] = 'indoStore';
		$this->load->view('template/header2', $data);	
		$this->load->view('customer/pesanan');
		$this->load->view('template/footer');
	}

	public function newslatter()
	{
		$data['judul'] = 'indoStore';
		$this->load->view('template/header2', $data);	
		$this->load->view('customer/newslatter');
		$this->load->view('template/footer');
	}	

	public function ulasan()
	{
		$data['judul'] = 'indoStore';
		$this->load->view('template/header2', $data);	
		$this->load->view('customer/ulasan');
		$this->load->view('template/footer');
	}	

	public function ulasan_detil()
	{
		$data['judul'] = 'indoStore';
		
		$this->load->view('template/header2', $data);	
		$this->load->view('customer/ulasan_detil');
		$this->load->view('template/footer');
	}

	public function wishlist()
	{
		$data['judul'] = 'indoStore';
		$this->load->view('template/header2', $data);	
		$this->load->view('customer/wishlist');
		$this->load->view('template/footer');
	}

	public function regis()
	{
		$data['judul'] = 'indoStore';
		$namad = $this->input->post('namadepan');
		$namab = $this->input->post('namabelakang');
		$email = $this->input->post('email');
		$pass = $this->input->post('password');
		$repass = $this->input->post('kpassword');
		/*if(isSama($pass, $repass)){*/
			$data1 = array(
				'namadepan' => $namad,
				'namabelakang' => $namab,
				'email' => $email,
				'password' =>$pass
			); 
			
			$table = 'user';
			$apaja = $this->LoginModel->register_user($table, $data1);
			if($apaja){
				$this->session->set_flashdata('alert', 'registrasi_berhasil');
			}else{
  				echo "<script>alert('Gagal Menambahkan Data');</script>";
			}
		/*}
		else{
			$this->session->set_flashdata('alert', 'Registrasi gagal, cek password!!!');
		}*/


		$this->load->view('template/header',$data);
		$this->load->view('customer/regis');
		$this->load->view('template/footer',$data);
	}
	public function isSama($pass, $kpass)
	{
		if($pass == $kpass){
			return true;
		}
		else{
			return false;
		}
	}

	public function login()
	{
	 # code...
	 	$email = $this->input->post('email');
		$password = $this->input->post('password');
   
		$user = $this->LoginModel->login_user($email, $password);
		$admin = $this->LoginModel->login_admin($email, $password);
   		$sess_data = array(/*inisialisasi session*/
			'logged_in' => '',
			'email' => ''
		);	
		if ($user) {/*jika usser yang melakukan login*/
		   	$sess_data['logged_in'] = 'User';
		   	$sess_data['email'] = $user->email;
		  	$this->session->set_userdata('logged_in', $sess_data['logged_in']);
		   	$this->session->set_userdata('email', $sess_data['email']);
		  	redirect('Ccustomer/profile');
		}
		else if($admin){/*jika admin yang melakukan login*/
	    	$sess_data['logged_in']= 'Admin';
		   	$sess_data['email'] = $admin->email;
		   $this->session->set_userdata('logged_in', $sess_data['logged_in']);
		   $this->session->set_userdata('email', $sess_data['email']);
		   redirect('Ccustomer/admin');
		 } 
		 else {

			echo "<script>alert('Gagal login: Cek email, password!');</script>";
			$data['judul'] = 'indoStore';
		 	$this->load->view('template/header', $data);
		 	$this->load->view('customer/account');
		 	$this->load->view('template/footer'); 
		}
	}
  
}
?>